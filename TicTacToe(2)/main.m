//
//  main.m
//  TicTacToe(2)
//
//  Created by apple on 13/10/15.
//  Copyright © 2015 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
