//
//  playerNameView.m
//  TicTacToe(2)
//
//  Created by apple on 13/10/15.
//  Copyright © 2015 apple. All rights reserved.
//

#import "playerNameView.h"
#import "startGameView.h"

@implementation playerNameView
{
    UITextField*nameTextField;
    UITextField*anotherNameTextField;
    UIImageView*imagePlayer;
    startGameView *startGameViewObject;
    
}

-(void) playerNameViewClass{
    [self firstplayerTextField];
    [self secondPlayerTextField];
    [self imageView];
    [self createBackButton];
    [self startButton];
   [self startButton];
}


#pragma marks- Create TextField

-(void) firstplayerTextField{
    nameTextField =  [[UITextField alloc] init];
    nameTextField.frame = CGRectMake(80,100, 200, 30);
    nameTextField.placeholder  = @"FirstPlayer";
    nameTextField.backgroundColor = [UIColor whiteColor];
    nameTextField.textColor = [UIColor blueColor];
    nameTextField.font = [UIFont systemFontOfSize:14.0f];
    nameTextField.borderStyle = UITextBorderStyleRoundedRect;
    nameTextField.textAlignment = NSTextAlignmentCenter;
    nameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _string=[nameTextField.text copy];
    //  nameTextField.delegate = self;
    [self addSubview:nameTextField];
    
    
}

-(void) secondPlayerTextField{
    anotherNameTextField =  [[UITextField alloc] init];
    anotherNameTextField.frame = CGRectMake(80,150, 200, 30);
    anotherNameTextField.placeholder  = @"SecondPlayer";
    anotherNameTextField.backgroundColor = [UIColor whiteColor];
    anotherNameTextField.textColor = [UIColor blueColor];
    anotherNameTextField.font = [UIFont systemFontOfSize:14.0f];
    anotherNameTextField.borderStyle = UITextBorderStyleRoundedRect;
    anotherNameTextField.textAlignment = NSTextAlignmentCenter;
    //anotherNameTextField.keyboardType=UIKeyboardTypeNumberPad;
    anotherNameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _string1=[nameTextField.text copy];
    //phnNumberTextfield.delegate = self;
    [self addSubview:anotherNameTextField];
    
    
}



#pragma marks- Create Image


-(void)imageView{
    imagePlayer=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"images2.jpg"]];
    imagePlayer.frame=CGRectMake(0, 250,320 ,318);
  //  imagePlayer.userInteractionEnabled = true;
   
    [self addSubview:imagePlayer];
    
}



#pragma marks - Create Button



-(void)createBackButton{
    UIButton*backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 20, 50, 50)];
    backButton.backgroundColor= [UIColor clearColor] ;
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [ backButton addTarget:self action:@selector(createBackButtonAction) forControlEvents:UIControlEventTouchUpInside];
    //[backButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self  addSubview:backButton];
}

-(void)startButton{
   UIButton *button1 = [[UIButton alloc]init];
    button1.frame=CGRectMake(130,200, 100, 30);
    button1.layer.borderWidth=2.0f;
    button1.tintColor = [UIColor yellowColor];
    button1.backgroundColor = [UIColor whiteColor];
    [ button1 setTitle:@"START" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [ button1 addTarget:self action:@selector(ButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button1];
}




#pragma marks - Button Action


-(void)createBackButtonAction{
    
  //  [self removeFromSuperview];
    
     [self.delegate playerNameViewMethodButton];
}

-(void)ButtonAction{
    
    
    
    if(![nameTextField.text isEqualToString:@""] || ![anotherNameTextField.text isEqualToString:@""])
    {
  
         [self endEditing:TRUE];
        
        _string=nameTextField.text;
        _string1=anotherNameTextField.text;
        
       
        
        [self.delegate playerNameViewMethod];
    
    
    
    }
    
    else if ([nameTextField.text isEqualToString:@""] || [anotherNameTextField.text isEqualToString:@""])
        
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"WARNING"
                                                        message:@"Please Enter The Names "
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:Nil, nil];
        
        [alert show];
        
    }
   
 
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:TRUE];
}

@end
