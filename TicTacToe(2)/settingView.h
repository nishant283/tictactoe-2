//
//  settingView.h
//  TicTacToe(2)
//
//  Created by apple on 14/10/15.
//  Copyright © 2015 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface settingView : UIView<AVAudioSessionDelegate>
{
    AVAudioPlayer*audioPlayerObject;
}
-(void) myAudioPlayer;

-(void) setttingViewClass;
@end
