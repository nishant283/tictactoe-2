//
//  startGameView.m
//  TicTacToe(2)
//
//  Created by apple on 14/10/15.
//  Copyright © 2015 apple. All rights reserved.
//

#import "startGameView.h"

@implementation startGameView{
  
    UIButton*Button1;
    UIButton*Button2;
    UIButton*Button3;
    UIButton*Button4;
    UIButton*Button5;
    UIButton*Button6;
    UIButton*Button7;
    UIButton*Button8;
    UIButton*Button9;
    int player;
   UIImageView *imageNmae;
    //UIImageView *XimageView;
    //UIImageView *ZeroImageView;
    
    
}


-(void) startGameViewClass{
    player=1;
    
    [self imageView];
    [self createBackButton];
    [self createLabel];
    [self createLabel1];
    
    [self createView];
    [self createView2];
    [self createView3];
    [self createView4];
    [self createPlayNowButton];
    //[self CreateXImageView];
    [self createButton1];
    [self createButton2];
    [self createButton3];
    [self createButton4];
    [self createButton5];
    [self createButton6];
    [self createButton7];
    [self createButton8];
    [self createButton9];
    
//    [self CreateXImageView];
//    [self CreateZeroImageView];
    [self CheckMatch];
    
}


#pragma marks-- CreateImageView

-(void)imageView{
    imageNmae=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"imageLast.jpg"]];
    imageNmae.frame=CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    imageNmae.userInteractionEnabled = true;
    
    [self addSubview:imageNmae];
    
}





#pragma Marks CreateButton

-(void)createBackButton{
    UIButton*backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 20, 50, 50)];
    backButton.backgroundColor= [UIColor clearColor] ;
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [ backButton addTarget:self action:@selector(createBackButtonAction) forControlEvents:UIControlEventTouchUpInside];
    //[backButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self  addSubview:backButton];
}


-(void)createPlayNowButton{
    UIButton*playButton=[[UIButton alloc]initWithFrame:CGRectMake(85, 52, 150, 80)];
    playButton.backgroundColor= [UIColor clearColor] ;
    [playButton setBackgroundImage:[UIImage imageNamed:@"playNow.png"] forState:UIControlStateNormal];
    [ playButton addTarget:self action:@selector(creatPlayNowButtonAction) forControlEvents:UIControlEventTouchUpInside];
    //[backButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self  addSubview:playButton];
}


-(void)createButton1{
    Button1=[[UIButton alloc]initWithFrame:CGRectMake(45, 135, 60, 60)];
    Button1.backgroundColor= [UIColor clearColor] ;
  //  [backButton setBackgroundImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [ Button1 addTarget:self action:@selector(createButton1Action) forControlEvents:UIControlEventTouchUpInside];
    //[backButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self  addSubview:Button1];
}


-(void)createButton2{
    Button2=[[UIButton alloc]initWithFrame:CGRectMake(125, 130, 65, 65)];
    Button2.backgroundColor= [UIColor clearColor] ;
   // [backButton setBackgroundImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [ Button2 addTarget:self action:@selector(createButton2Action) forControlEvents:UIControlEventTouchUpInside];
    //[backButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self  addSubview:Button2];
}

-(void)createButton3{
    Button3=[[UIButton alloc]initWithFrame:CGRectMake(210, 130, 65, 65)];
    Button3.backgroundColor= [UIColor clearColor] ;
    //[backButton setBackgroundImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [ Button3 addTarget:self action:@selector(createButton3Action) forControlEvents:UIControlEventTouchUpInside];
    //[backButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self  addSubview:Button3];
}

-(void)createButton4{
    Button4=[[UIButton alloc]initWithFrame:CGRectMake(40, 220, 65, 65)];
    Button4.backgroundColor= [UIColor clearColor] ;
   // [backButton setBackgroundImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [ Button4 addTarget:self action:@selector(createButton4Action) forControlEvents:UIControlEventTouchUpInside];
    //[backButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self  addSubview:Button4];
}

-(void)createButton5{
    Button5=[[UIButton alloc]initWithFrame:CGRectMake(125, 220, 65, 65)];
    Button5.backgroundColor= [UIColor clearColor] ;
   // [backButton setBackgroundImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [ Button5 addTarget:self action:@selector(createButton5Action) forControlEvents:UIControlEventTouchUpInside];
    //[backButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self  addSubview:Button5];
}



-(void)createButton6{
    Button6=[[UIButton alloc]initWithFrame:CGRectMake(210, 220, 65, 65)];
    Button6.backgroundColor= [UIColor clearColor] ;
    //[backButton setBackgroundImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [ Button6 addTarget:self action:@selector(createButton6Action) forControlEvents:UIControlEventTouchUpInside];
    //[backButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self  addSubview:Button6];
}


-(void)createButton7{
    Button7=[[UIButton alloc]initWithFrame:CGRectMake(40, 300, 65, 65)];
    Button7.backgroundColor= [UIColor clearColor] ;
  //  [backButton setBackgroundImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [ Button7 addTarget:self action:@selector(createButton7Action) forControlEvents:UIControlEventTouchUpInside];
    //[backButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self  addSubview:Button7];
}

-(void)createButton8{
    Button8=[[UIButton alloc]initWithFrame:CGRectMake(120, 300, 65, 65)];
    Button8.backgroundColor= [UIColor clearColor] ;
   // [backButton setBackgroundImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [ Button8 addTarget:self action:@selector(createButton8Action) forControlEvents:UIControlEventTouchUpInside];
    //[backButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self  addSubview:Button8];
}

-(void)createButton9{
    Button9=[[UIButton alloc]initWithFrame:CGRectMake(210, 300, 65, 65)];
    Button9.backgroundColor= [UIColor clearColor] ;
   // [backButton setBackgroundImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [ Button9 addTarget:self action:@selector(createButton9Action) forControlEvents:UIControlEventTouchUpInside];
    //[backButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self  addSubview:Button9];
}

-(void)playerSwitch{
    if (player==1) {
        player=2;
        
    } else{ player=1;}
}

#pragma marks-- Label

-(void) createLabel


{
    _fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 470, 85 , 85)];
     //fromLabel.clipsToBounds = YES;
    _fromLabel.layer.cornerRadius=_fromLabel.bounds.size.width/2.0;
    _fromLabel.layer.masksToBounds=YES;

     _fromLabel.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;
    _fromLabel.textColor = [UIColor whiteColor];
    _fromLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_fromLabel];
    
    
  
}
-(void) createLabel1
{
    _fromlabel1 = [[UILabel alloc]initWithFrame:CGRectMake(225, 470, 85 , 85)];
  //  fromLabel.clipsToBounds = YES;
    _fromlabel1.layer.cornerRadius=_fromlabel1.bounds.size.width/2.0;
    _fromlabel1.layer.masksToBounds=YES;

     _fromlabel1.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;
    _fromlabel1.textColor = [UIColor whiteColor];
     _fromlabel1.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_fromlabel1];
    
}



#pragma marks ButtonAction

-(void)createBackButtonAction{
    //[self removeFromSuperview];
    [self.delegate startGameMethod];
}

-(void)creatPlayNowButtonAction{
    [Button1 setImage:nil forState:UIControlStateNormal];
    [Button2 setImage:nil forState:UIControlStateNormal];
    [Button3 setImage:nil forState:UIControlStateNormal];
    [Button4 setImage:nil forState:UIControlStateNormal];
    [Button5 setImage:nil forState:UIControlStateNormal];
    [Button6 setImage:nil forState:UIControlStateNormal];
    [Button7 setImage:nil forState:UIControlStateNormal];
    [Button8 setImage:nil forState:UIControlStateNormal];
    [Button9 setImage:nil forState:UIControlStateNormal];
    
    [self.delegate tictactoerestart ];
    
}





-(void)createButton1Action{
    if (!Button1.imageView.image) {
        if (player==2) {
            [Button1 setImage:[UIImage imageNamed:@"zero.png"] forState:UIControlStateNormal];
              [self playerSwitch];
              [self CheckMatch];
            
                 }
       
        
    }
    else if(player==1)
    {
    [Button1 setImage:[UIImage imageNamed:@"xImage.png"] forState:UIControlStateNormal];    }
    [self playerSwitch];
    [self CheckMatch];

}

-(void)createButton2Action{
    
        if (!Button2.imageView.image) {
            
            if (player==1)
            {
                [Button2 setImage:[UIImage imageNamed:@"zero.png" ]forState:UIControlStateNormal];
                [self playerSwitch];
               [self CheckMatch];
            }
            else if (player==2)
            {
                [Button2 setImage:[UIImage imageNamed:@"xImage.png" ]forState:UIControlStateNormal];
                [self playerSwitch];
                [self CheckMatch];
                
                
            }
        }
    }
    



-(void)createButton3Action{
    
        if (!Button3.imageView.image) {
            
            if (player==1)
            {
                [Button3 setImage:[UIImage imageNamed:@"zero.png" ]forState:UIControlStateNormal];
                [self playerSwitch];
                [self CheckMatch];
            }
            else if (player==2)
            {
                [Button3 setImage:[UIImage imageNamed:@"xImage.png" ]forState:UIControlStateNormal];
                [self playerSwitch];
                [self CheckMatch];
                
                
            }
        }
    }



-(void)createButton4Action{
    
        if (!Button4.imageView.image) {
            
            if (player==1)
            {
                [Button4 setImage:[UIImage imageNamed:@"zero.png" ]forState:UIControlStateNormal];
                [self playerSwitch];
                [self CheckMatch];
            }
            else if (player==2)
            {
                [Button4 setImage:[UIImage imageNamed:@"xImage.png" ]forState:UIControlStateNormal];
                [self playerSwitch];
              [self CheckMatch];
                
                
            }
        }
    
}


-(void)createButton5Action{
    
        if (!Button5.imageView.image) {
            
            if (player==1)
            {
                [Button5 setImage:[UIImage imageNamed:@"zero.png" ]forState:UIControlStateNormal];
                [self playerSwitch];
                [self CheckMatch];
            }
            else if (player==2)
            {
                [Button5 setImage:[UIImage imageNamed:@"xImage.png" ]forState:UIControlStateNormal];
                [self playerSwitch];
               [self CheckMatch];
            
                
            }
        }
    
}


-(void)createButton6Action{
    
        if (!Button6.imageView.image) {
            
            if (player==1)
            {
                [Button6 setImage:[UIImage imageNamed:@"zero.png" ]forState:UIControlStateNormal];
                [self playerSwitch];
            [self CheckMatch];
            }
            else if (player==2)
            {
                [Button6 setImage:[UIImage imageNamed:@"xImage.png" ]forState:UIControlStateNormal];
                [self playerSwitch];
              [self CheckMatch];
                
                
            }
        }
    
}


-(void)createButton7Action{
    
    if (!Button7.imageView.image) {
        
        if (player==1)
        {
            [Button7 setImage:[UIImage imageNamed:@"zero.png" ]forState:UIControlStateNormal];
            [self playerSwitch];
                 [self CheckMatch];
        }
        else if (player==2)
        {
            [Button7 setImage:[UIImage imageNamed:@"xImage.png" ]forState:UIControlStateNormal];
            [self playerSwitch];
            [self CheckMatch];
            
            
        }
    }
    
}


-(void)createButton8Action{
    
    if (!Button8.imageView.image) {
        
        if (player==1)
        {
            [Button8 setImage:[UIImage imageNamed:@"zero.png" ]forState:UIControlStateNormal];
            [self playerSwitch];
                  [self CheckMatch];
        }
        else if (player==2)
        {
            [Button8 setImage:[UIImage imageNamed:@"xImage.png" ]forState:UIControlStateNormal];
            [self playerSwitch];
            [self CheckMatch];
            
            
        }
    }
    
}

-(void)createButton9Action{
    
    if (!Button9.imageView.image) {
        
        if (player==1)
        {
            [Button9 setImage:[UIImage imageNamed:@"zero.png" ]forState:UIControlStateNormal];
            [self playerSwitch];
                [self CheckMatch];
        }
        else if (player==2)
        {
            [Button9 setImage:[UIImage imageNamed:@"xImage.png" ]forState:UIControlStateNormal];
            [self playerSwitch];
               [self CheckMatch];
            
            
        }
    }
    
}





#pragma marks - CreateImageView


//-(void)CreateXImageView{
////    XimageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"xImage.png"]];
////    XimageView .frame=CGRectMake(50, 200, 50, 50);
////    
////    [self addSubview:XimageView];
//
//}
//
//
//-(void)CreateZeroImageView{
//    
//}

#pragma marks- Create View


-(void)createView{
    UIView*lineView=[[UIView alloc]initWithFrame:CGRectMake(35, 200, 250, 5)];
    lineView.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;
    [self addSubview:lineView];
    
}

-(void)createView2{
    UIView*lineView=[[UIView alloc]initWithFrame:CGRectMake(35, 290, 250, 5)];
    lineView.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;
    [self addSubview:lineView];
    
}


-(void)createView3{
    UIView*lineView=[[UIView alloc]initWithFrame:CGRectMake(110, 120, 5, 250)];
    lineView.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;
    [self addSubview:lineView];
    
}


-(void)createView4{
    UIView*lineView=[[UIView alloc]initWithFrame:CGRectMake(200, 120, 5, 250)];
    lineView.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;
    [self addSubview:lineView];
    
}


#pragma marks Cross and Zero Conditions 



-(void)CheckMatch
{
    
    if ([Button1.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button2.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button3.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]]){
        NSLog(@"x1 match");
        
        UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(30, 170,250, 3)];
        tempView.backgroundColor = [UIColor redColor];
        [self addSubview:tempView];
        
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
        
    }else if([Button4.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button5.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button6.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]]){
        NSLog(@"x1 match");
        
        UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(30, 250,250, 3)];
        tempView.backgroundColor = [UIColor redColor];
        [self addSubview:tempView];
        
        
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
        
    }else if([Button7.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button8.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button9.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]]){
        NSLog(@"x3 match");
        
        UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(30, 330,250, 3)];
        tempView.backgroundColor = [UIColor redColor];
        [self addSubview:tempView];
        
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
    }
    else if ([Button1.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button4.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button7.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]]){
        NSLog(@"x4 match");
        
        UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(65, 115,3, 250)];
        tempView.backgroundColor = [UIColor redColor];
        [self addSubview:tempView];
        
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
    }
    
    else if ([Button2.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button5.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button8.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]]){
        NSLog(@"x5 match");
        
        UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(160, 115,3, 250)];
        tempView.backgroundColor = [UIColor redColor];
        [self addSubview:tempView];
        
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
    }
    
    else if([Button3.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button6.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button9.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]]){
        NSLog(@"x6 match");
        
        
        UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(240, 115,3, 250)];
        tempView.backgroundColor = [UIColor redColor];
        [self addSubview:tempView];
        
        
        
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
    }
    
    else  if ([Button1.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button5.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button9.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]]){
        NSLog(@"x7 match");
        
        UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
        [pathHorizontal1 moveToPoint:CGPointMake(35, 125)];
        [pathHorizontal1 addLineToPoint:CGPointMake(290, 380)];
        CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
        shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
        shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
        shapeLayerHorizontal1.lineWidth = 3.0;
        shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
        [self.layer addSublayer:shapeLayerHorizontal1];
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
    }
    
    else if([Button3.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button5.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]] && [Button7.imageView.image isEqual:[UIImage imageNamed:@"zero.png"]])
    {
        UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
        [pathHorizontal1 moveToPoint:CGPointMake(280, 120)];
        [pathHorizontal1 addLineToPoint:CGPointMake(20.0, 380)];
        CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
        shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
        shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
        shapeLayerHorizontal1.lineWidth = 3.0;
        shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
        [self.layer addSublayer:shapeLayerHorizontal1];
        
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
        NSLog(@"x8 match");
    }
    
#pragma mark---cross image actions...
    
    
    else if ([Button1.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button2.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button3.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]]){
        NSLog(@"x1 match");
        
        
        UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(30, 170,250, 3)];
        tempView.backgroundColor = [UIColor redColor];
        [self addSubview:tempView];
        
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
        
    }else if([Button4.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button5.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button6.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]]){
        NSLog(@"x2 match");
        
        UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(30, 250,250, 3)];
        tempView.backgroundColor = [UIColor redColor];
        [self addSubview:tempView];
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
        
    }else if([Button7.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button8.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button9.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]]){
        NSLog(@"x3 match");
        
        UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(30, 330,250, 3)];
        tempView.backgroundColor = [UIColor redColor];
        [self addSubview:tempView];
        
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
        
    }
    else if ([Button1.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button4.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button7.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]]){
        NSLog(@"x4 match");
        
        UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(65, 115,3, 250)];
        tempView.backgroundColor = [UIColor redColor];
        [self addSubview:tempView];
        
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
        
    }
    
    else if ([Button2.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button5.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button8.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]]){
        NSLog(@"x5 match");
        
        UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(160, 115,3, 250)];
        tempView.backgroundColor = [UIColor redColor];
        [self addSubview:tempView];
        
        
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
        
    }
    
    else if([Button3.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button6.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button9.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]]){
        NSLog(@"x6 match");
        
        
        UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(240, 115,3, 250)];
        tempView.backgroundColor = [UIColor redColor];
        [self addSubview:tempView];
        
        
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
    }
    
    else  if ([Button1.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button5.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button9.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]]){
        NSLog(@"x7 match");
        
        UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
        [pathHorizontal1 moveToPoint:CGPointMake(35, 125)];
        [pathHorizontal1 addLineToPoint:CGPointMake(290, 380)];
        CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
        shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
        shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
        shapeLayerHorizontal1.lineWidth = 3.0;
        shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
        [self.layer addSublayer:shapeLayerHorizontal1];
        
        
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
    }
    
    else if([Button3.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button5.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]] && [Button7.imageView.image isEqual:[UIImage imageNamed:@"xImage.png"]])
    {
        UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
        [pathHorizontal1 moveToPoint:CGPointMake(280, 120)];
        [pathHorizontal1 addLineToPoint:CGPointMake(20.0, 380)];
        CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
        shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
        shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
        shapeLayerHorizontal1.lineWidth = 3.0;
        shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
        [self.layer addSublayer:shapeLayerHorizontal1];
        
        
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
    }
    
}




@end
