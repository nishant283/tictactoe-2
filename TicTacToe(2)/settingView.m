//
//  settingView.m
//  TicTacToe(2)
//
//  Created by apple on 14/10/15.
//  Copyright © 2015 apple. All rights reserved.
//

#import "settingView.h"

@implementation settingView
{
    UISwitch *switch3;
}

-(void) setttingViewClass{
    [self CreateimageView];
    [self createBackButton3];
    [self createLabelSound];
    [self createLabelTheme1];
    [self createLabelTheme2];
//    UISwitch *onoff = [[UISwitch alloc] initWithFrame: 20, 280, 80, 80];
//    [onoff addTarget: self action: @selector(flip:) forControlEvents:UIControlEventValueChanged];
    [self createSwitchSound];
    [self createSwitchTheme1];
    [self createSwitchTheme2];
    [self myAudioPlayer];
   
}

#pragma marks CreateImageView

-(void) CreateimageView{
    UIImageView *imageName=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"setting2.png"]];
    imageName.frame=CGRectMake(85, 52, 160, 140);
    imageName.userInteractionEnabled = true;
    
    [self addSubview:imageName];
    
}

#pragma marks Create Button

-(void)createBackButton3{
    UIButton*backButton3=[[UIButton alloc]initWithFrame:CGRectMake(0, 20, 50, 50)];
    backButton3.backgroundColor= [UIColor clearColor] ;
    [backButton3 setBackgroundImage:[UIImage imageNamed:@"backbutton3.png"] forState:UIControlStateNormal];
    [ backButton3 addTarget:self action:@selector(createBackButton3Action) forControlEvents:UIControlEventTouchUpInside];
    //[backButton setTitle:@"DONE" forState:UIControlStateNormal];
    
    [self  addSubview:backButton3];
}

#pragma marks ButtonAction

-(void) createBackButton3Action{
    [self removeFromSuperview];
    
}

#pragma marks CreateLabel

-(void)createLabelSound{
    
    UILabel *label1=[[UILabel alloc]init];
    label1.frame =CGRectMake(20, 280, 80, 80);
    label1.text = @"Sound";
    label1.layer.cornerRadius=label1.bounds.size.width/2.0;
    label1.layer.masksToBounds=YES;
    label1.textAlignment=NSTextAlignmentCenter;
    label1.textColor=[UIColor whiteColor];
    label1.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(80/255.0) blue:(90/255.0) alpha:0.8] ;
   
    [self addSubview:label1];
}


-(void)createLabelTheme1{
    
    UILabel *label2=[[UILabel alloc]init];
    label2.frame =CGRectMake(20, 380, 80, 80);
    label2.text = @"Theme1";
    label2.layer.cornerRadius=label2.bounds.size.width/2.0;
    label2.layer.masksToBounds=YES;
    label2.textAlignment=NSTextAlignmentCenter;
    label2.textColor=[UIColor whiteColor];
    label2.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(80/255.0) blue:(90/255.0) alpha:0.8] ;
    [self addSubview:label2];
}


-(void)createLabelTheme2{
    
    UILabel *label3=[[UILabel alloc]init];
    label3.frame =CGRectMake(20, 480, 80, 80);
    label3.text = @"Theme2";
    label3.layer.cornerRadius=label3.bounds.size.width/2.0;
    label3.layer.masksToBounds=YES;
    label3.textColor=[UIColor whiteColor];
    label3.textAlignment=NSTextAlignmentCenter;
    label3.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(80/255.0) blue:(90/255.0) alpha:0.8] ;
    [self addSubview:label3];
}

#pragma marks Create Switches


-(void) createSwitchSound{
    UISwitch *switch1= [[UISwitch alloc]init];
    switch1.frame=CGRectMake(190, 300, 80, 80);
    [switch1 addTarget: self action: @selector(switchSoundAction:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:switch1];
    
}

-(void) createSwitchTheme1{
    UISwitch *switch2= [[UISwitch alloc]init];
    switch2.frame=CGRectMake(190, 400, 80, 80);
    [switch2 addTarget: self action: @selector(switchTheme1Action) forControlEvents:UIControlEventValueChanged];
    [self addSubview:switch2];
}

-(void) createSwitchTheme2{
    switch3= [[UISwitch alloc]init];
    
   [audioPlayerObject stop];
    switch3.frame=CGRectMake(190, 500, 80, 80);
    [switch3 addTarget: self action: @selector(switchTheme2Action) forControlEvents:UIControlEventValueChanged];
    [self addSubview:switch3];
}


#pragma marks SwitchesAction

-(void) switchSoundAction:(UISwitch *)sender{
    if( sender.on ==TRUE){
    NSURL *urlObject =[NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"Kremlin_Chimes_freetone.at.ua" ofType:@"mp3"]];
    audioPlayerObject =[[AVAudioPlayer alloc]initWithContentsOfURL:urlObject error:Nil] ;
    [[AVAudioSession sharedInstance]setCategory:AVAudioSessionCategoryPlayback error:Nil];
    [[UIApplication sharedApplication]beginReceivingRemoteControlEvents];
    audioPlayerObject.volume=5;
    [audioPlayerObject setNumberOfLoops:-1];
    [audioPlayerObject play];
    
    }
    
        else {
            
        [audioPlayerObject stop];
            
            
            
    }
    


}

-(void)switchTheme1Action{
    
}

-(void)switchTheme2Action{
    
}

#pragma marks CreateAudio 

-(void)myAudioPlayer{
//    NSURL *urlObject =[NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"Kremlin_Chimes_freetone.at.ua" ofType:@"mp3"]];
//    audioPlayerObject =[[AVAudioPlayer alloc]initWithContentsOfURL:urlObject error:Nil] ;
//    [[AVAudioSession sharedInstance]setCategory:AVAudioSessionCategoryPlayback error:Nil];
//    [[UIApplication sharedApplication]beginReceivingRemoteControlEvents];
//    audioPlayerObject.volume=5;
//    [audioPlayerObject setNumberOfLoops:-1];
//    [audioPlayerObject play];
    
}


@end
