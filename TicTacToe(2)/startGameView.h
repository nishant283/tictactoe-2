//
//  startGameView.h
//  TicTacToe(2)
//
//  Created by apple on 14/10/15.
//  Copyright © 2015 apple. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol startGameClass <NSObject>


-(void)startGameMethod;
-(void) tictactoerestart;


@end
@interface startGameView : UIView

-(void) startGameViewClass;


@property (nonatomic, assign) id <startGameClass> delegate;
@property (nonatomic, strong) UILabel *fromLabel;
@property (nonatomic, strong) UILabel * fromlabel1;


@end
