//
//  playerView.m
//  TicTacToe(2)
//
//  Created by apple on 13/10/15.
//  Copyright © 2015 apple. All rights reserved.
//

#import "playerView.h"
#import "playerNameView.h"
#import "settingView.h"

@implementation playerView
{
    UIButton*button1;
    UIButton*button2;
    UIButton*button3;
    UIImageView*imageNmae;
    playerNameView *playerNameViewObject;
    settingView * settingViewObject;
}
-(void) playerViewClass{
     [self imageView];
    [self startButton];
    [self settingButton];
    [self scoreButton];

    



}



#pragma marks- Create Button

-(void)startButton{
    button1 = [[UIButton alloc]init];
    button1.frame=CGRectMake(110,130, 100, 30);
    button1.layer.borderWidth=2.0f;
    button1.tintColor = [UIColor yellowColor];
    button1.backgroundColor = [UIColor brownColor];
    [ button1 setTitle:@"START" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ button1 addTarget:self action:@selector(startButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [imageNmae addSubview:button1];
}

-(void)settingButton{
    button2 = [[UIButton alloc]init];
    button2.frame=CGRectMake(0,250, 100, 30);
    
    button2.layer.borderWidth=2.0f;
    button2.tintColor = [UIColor blackColor];
    button2.backgroundColor = [UIColor brownColor];
    [ button2 setTitle:@"SETTING" forState:UIControlStateNormal];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ button2 addTarget:self action:@selector(settingButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [imageNmae addSubview:button2];
    
}



-(void)scoreButton{
    button3 = [[UIButton alloc]init];
    button3.frame=CGRectMake(230,250, 100, 30);
    button3.layer.borderWidth=2.0f;

    button3.tintColor = [UIColor blackColor];
    button3.backgroundColor = [UIColor brownColor];
    [ button3 setTitle:@"SCORE" forState:UIControlStateNormal];
    [button3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ button3 addTarget:self action:@selector(scoreButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [imageNmae addSubview:button3];
    
}

#pragma marks- ButtonAction


-(void)startButtonAction{
    

    
    [self.delegate playerViewClass23];
    
}

-(void)settingButtonAction{
    settingViewObject=[[settingView alloc]init];
    settingViewObject.frame=CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    settingViewObject.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;
    [settingViewObject setttingViewClass];
    [self addSubview:settingViewObject];
    
}

-(void)scoreButtonAction{
    
}


#pragma marks- CreateImagevview


-(void)imageView{
    imageNmae=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tictactoeimg.jpg"]];
        imageNmae.frame=CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    imageNmae.userInteractionEnabled = true;
          
        [self addSubview:imageNmae];

}

-(void)passValue:(UIButton *)value{
    //playerNameViewObject.delegate=self;
    
}


@end
