//
//  ViewController.m
//  TicTacToe(2)
//
//  Created by apple on 13/10/15.
//  Copyright © 2015 apple. All rights reserved.
//

#import "ViewController.h"
#import "playerView.h"
#import "playerNameView.h"
#import "startGameView.h"

@interface ViewController ()<playerViewClass2, playerNameViewPass,startGameClass>

@end

@implementation ViewController
{
    playerView*playerViewObject;
   //playerView *playerViewObject2;
    playerNameView*playerNames;
    startGameView*startGameViewObject;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self playerViews];
    playerViewObject.delegate=self;
   // playerViewObject2.delegate=self;
    
    }


-(void)playerViews{
    playerViewObject=[[playerView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
   // playerViewObject.backgroundColor=[UIColor grayColor];
    playerViewObject.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;

    [ playerViewObject playerViewClass];
    
    [self.view addSubview:playerViewObject];
    
    
    
    
    
}

-(void)playerViewClass23{
    playerNames=[[playerNameView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height)];
    playerNames.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;
    [ playerNames playerNameViewClass];
    playerNames.delegate=self;
    [self.view addSubview:playerNames];
    
}

-(void)playerNameViewMethod{
   
    startGameViewObject=[[startGameView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height)];
    
    [startGameViewObject startGameViewClass];
    startGameViewObject.fromLabel.text=playerNames.string;
    startGameViewObject.fromlabel1.text=playerNames.string1;
     startGameViewObject.delegate=self;
   
    [self.view addSubview: startGameViewObject];
    
}



-(void)playerNameViewMethodButton{
    playerViewObject=[[playerView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    // playerViewObject.backgroundColor=[UIColor grayColor];
    playerViewObject.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;
    
    [ playerViewObject playerViewClass];
    
    playerViewObject.delegate=self;
    
 [self.view addSubview:playerViewObject];

}



-(void)startGameMethod{
    
    playerViewObject=[[playerView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    // playerViewObject.backgroundColor=[UIColor grayColor];
    playerViewObject.backgroundColor= [UIColor colorWithRed:(0/255.0) green:(60/255.0) blue:(100/255.0) alpha:1.0] ;
    
    [ playerViewObject playerViewClass];
    playerViewObject.delegate=self;
    
    [self.view addSubview:playerViewObject];
    
}


-(void)tictactoerestart{
    
    startGameViewObject=[[startGameView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,self.view.frame.size.height)];
    
    [startGameViewObject startGameViewClass];
    startGameViewObject.fromLabel.text=playerNames.string;
    startGameViewObject.fromlabel1.text=playerNames.string1;
    startGameViewObject.delegate=self;
    
    [self.view addSubview: startGameViewObject];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
