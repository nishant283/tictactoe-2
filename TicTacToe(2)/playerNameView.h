//
//  playerNameView.h
//  TicTacToe(2)
//
//  Created by apple on 13/10/15.
//  Copyright © 2015 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol playerNameViewPass <NSObject>


-(void)playerNameViewMethod;

//@protocol PlayerNameViewPass2 <NSObject>

-(void)playerNameViewMethodButton;



@end

@interface playerNameView : UIView
-(void) playerNameViewClass;

@property (nonatomic, assign) id <playerNameViewPass> delegate;
@property(nonatomic, strong) NSString *string;
@property (nonatomic,strong) NSString *string1;


@end
